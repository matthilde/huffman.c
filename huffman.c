/*
 * huffman.c by matthilde
 *
 * Lossless data compression using the Huffman Coding. Just as a
 * small programming challenge.
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;

const char *_magic_number = "HF.C";
static int isValidFile(FILE* f) {
    for (const char *s = _magic_number; *s; ++s)
        if (fgetc(f) != *s) return 0;
    return 1;
}
static void writeMagicNumber(FILE* f) {
    for (const char *s = _magic_number; *s; ++s) fputc(*s, f);
}

inline static void die(const char* message) {
    fprintf(stderr, "ERROR: %s\n", message); exit(1);
}
inline static void pdie(const char* message) {
    perror(message); exit(1);
}

typedef struct Node {
    enum { NODE_EMPTY, NODE_PARENT, NODE_ELEMENT, NODE_LIST } type;
    long weight;
    
    union {
        byte element;
        struct { struct Node* left, *right; } p;
    } c;

    struct Node *next; // linked list stuff
} Node;
typedef struct Symbol {
    long  length;
    byte  *bits;
} Symbol;

long   charcount[256] = { 0 };
Symbol symbols[256] = { (Symbol){ .length = 0 } };
Node   nodebuffer[768] = { (Node){ .type = NODE_EMPTY } };

/* Turns out this function is useless...
static Node* llNth(Node* l, int nth) {
    for (int i = 0; i <= nth; ++i) {
        if (!l->next) die("List index out of range");
        l = l->next;
    }
    return l;
}
*/
static void llInsert(Node *dest, Node *src, int nth) {
    Node *current = dest, *prev;
    for (int i = 0; i <= nth; ++i) {
        prev = current;
        if (!prev) die("List index out of range");
        current = current->next;
    }
    prev->next = src; src->next = current;
}
static Node* llPop(Node *l, int nth) {
    Node *current = l, *prev;
    for (int i = 0; i <= nth; ++i) {
        prev = current;
        current = current->next;
        if (!current) die("List index out of range");
    }
    prev->next = current->next;
    current->next = NULL;
    return current;
}
static void llAppend(Node *dest, Node *src) {
    Node *l = dest;
    while (l->next) l = l->next;
    l->next = src;
}
static Node* newNode(int type) {
    for (int i = 0; i < 512; ++i)
        if (nodebuffer[i].type == NODE_EMPTY) {
            nodebuffer[i].type = type;
            if (type == NODE_PARENT)
                nodebuffer[i].c.p.left = nodebuffer[i].c.p.right = NULL;
            return (nodebuffer + i);
        }
    die("No more node available."); return (Node*)69420;
}

// Printing stuff for debugging
static void printNode(Node *n);
static void printNList(Node *l) {
    putchar('[');
    for (Node *n = l->next; n; n = n->next) {
        printf("    "); printNode(n);
        if (n->next) printf(", ");
    }
    putchar(']');
}
static void printNode(Node* n) {
    switch (n->type) {
    case NODE_EMPTY:  printf("Node()"); break;
    case NODE_PARENT:
        printf("Node[%ld](", n->weight);
        printNode(n->c.p.left); putchar(' '); printNode(n->c.p.right);
        putchar(')'); break;
    case NODE_ELEMENT:
        printf("Node[%ld](%c %02x)",
                    n->weight, n->c.element, n->c.element);
        break;
    case NODE_LIST: printNList(n); break;
    }
    putchar('\n');
}
static void printTree(Node *n, int indent) {
    for (int i = 0; i < indent; ++i) putchar(' ');
    if (n->type == NODE_PARENT) {
        printf("- Parent [weight = %ld]\n", n->weight);
        printTree(n->c.p.left, indent + 4);
        printTree(n->c.p.right, indent + 4);
    } else {
        printf("- "); printNode(n);
    }
}
static void printTable() {
    Symbol *s;
    for (int i = 0; i < 256; ++i) {
        s = (symbols + i);
        if (s->length) {
            printf("%c ", i);
            for (int j = 0; j < s->length; ++j)
                putchar('0' + s->bits[j]);
            putchar('\n');
        }
    }
}

/////// HUFFMAN COMPRESSION STUFF
/// File i/o stuff
static void writeSymbolBits(FILE* f, Symbol* s) {
    byte *bits = s->bits; long length = s->length;
    byte bitbuf = 0;
    for (int i = 0; i < length; ++i) {
        if (i > 0 && (i & 7) == 0) {
            fputc(bitbuf, f); bitbuf = 0;
        }
        bitbuf |= bits[i] << (i & 7);
    }
    fputc(bitbuf, f);
}
static void writeTableToFile(FILE* f) {
    Symbol *s = symbols;
    // Define the table size
    int sz = 0;
    for (int i = 0; i < 256; ++i) sz += s[i].length > 0; fputc(sz, f);

    // Write the symbol definitions
    for (int i = 0; i < 256; ++i) {
        if (s->length) {
            fputc(i, f); fputc(s->length & 0xff, f);
            writeSymbolBits(f, s);
        }
        s++;
    }
}
static void writeContentToFile(FILE* inf, FILE* outf) {
    int c; Symbol *s;
    byte bitbuf = 0, bitindex = 0;
    while ((c=fgetc(inf)) != EOF) {
        s = (symbols + c);
        for (int i = 0; i < s->length; ++i) {
            if (bitindex == 8) {
                fputc(bitbuf, outf);
                bitindex = bitbuf = 0;
            }
            bitbuf |= s->bits[i] << bitindex;
            bitindex++;
        }
    }
    fputc(bitbuf, outf);
}
static void generateCompressedFile(FILE* inf, FILE* outf) {
    // Magic number!!!
    writeMagicNumber(outf);
    // Determine the size of the file...
    long osize = ftell(inf);
    fseek(inf, 0, SEEK_END); uint32_t size = ftell(inf);
    fseek(inf, osize, SEEK_SET);
    fwrite(&size, sizeof(size), 1, outf);
    // Write the symbols table to the file
    writeTableToFile(outf);
    // Encode and write the content of the file
    writeContentToFile(inf, outf);
}

/// Sorting and tree generation stuff
// This sorting function is now completely pointless...
/*
void sortList(Node **l) {
    // Time for an amazing O(n^2) sorting algorithm
    // The maximum of elements is 256 anyway.
    Node *arr = *l, *narr = newNode(NODE_LIST), *c;
    long m, mi;
    while (arr->next) {
        c = arr->next; m = c->weight; mi = 0;
        for (int i = 0; c; ++i) {
            if (c->weight < m) {
                m = c->weight; mi = i;
            }
            c = c->next;
        }
        llAppend(narr, llPop(arr, mi));
    }
    *l = narr; arr->type = NODE_EMPTY;
}
*/
static void insertNodeByWeight(Node *l, Node *n) {
    Node *c = l->next; int weight = n->weight, i;
    if (!c) llAppend(l, n);
    else {
        for (i = 0; c; ++i) {
            if (c->weight > weight) break;
            c = c->next;
        }
        llInsert(l, n, i);
    }
}
// returns 1 if it created a parent, 0 if it didn't
static int createParent(Node *l) {
    if (!l->next || !l->next->next) return 0;
    Node *na = llPop(l, 0); Node *nb = llPop(l, 0);
    Node *parent = newNode(NODE_PARENT);
    parent->weight = na->weight + nb->weight;
    parent->c.p.left = na; parent->c.p.right = nb;

    insertNodeByWeight(l, parent);
    return 1;
}
// Time to generate the tree
static void generateTree(Node *l) {
    while (createParent(l));
}
// Then you generate the table
static void _generateTable(Node *n, byte* buffer, long ptr) {
    Symbol *s;
    switch (n->type) {
    case NODE_PARENT:
        buffer[ptr] = 0;
        _generateTable(n->c.p.left, buffer, ptr + 1);
        buffer[ptr] = 1;
        _generateTable(n->c.p.right, buffer, ptr + 1);
        break;
    case NODE_ELEMENT:
        s = (symbols + n->c.element);
        s->length = ptr;
        s->bits = (byte*)malloc(sizeof(byte) * ptr);
        s->bits = memcpy(s->bits, buffer, ptr);
        break;
    default: die("???"); break;
    }
}
static void generateTable(Node *l) {
    if (!l->next) return;
    Node *n = l->next;
    byte buf[512];

    _generateTable(n, buf, 0);
}
static void freeTable() {
    for (int i = 0; i < 256; ++i)
        if (symbols[i].length) free(symbols[i].bits);
}

/////// DECOMPRESSION STUFF

static void readSymbolContent(FILE *f, Symbol *s) {
    byte bitbuf = fgetc(f), bitpos = 0;
    for (int i = 0; i < s->length; ++i) {
        if (bitpos == 8) { bitpos = 0; bitbuf = fgetc(f); }
        s->bits[i] = (bitbuf & (1 << bitpos)) > 0;
        ++bitpos;
    }
}
static void readFileInfo(FILE* f, uint32_t* size) {
    fread(size, sizeof(*size), 1, f); // File size

    int tablesize = fgetc(f); Symbol *s;
    for (int i = 0; i < tablesize; ++i) {
        s = (symbols + fgetc(f));
        s->length = fgetc(f);
        s->bits = (byte*)malloc(sizeof(byte) * s->length);
        readSymbolContent(f, s);
    }
}
static void insertSymbolInTree(Node* master, char ch, Symbol* s) {
    Node *n = master; byte bit;
    for (long i = 0; i < s->length - 1; ++i) {
        bit = s->bits[i];
        if (bit == 0) {
            if (!n->c.p.left) n->c.p.left = newNode(NODE_PARENT);
            n = n->c.p.left;
        } else {
            if (!n->c.p.right) n->c.p.right = newNode(NODE_PARENT);
            n = n->c.p.right;
        }
    }
    bit = s->bits[s->length - 1];
    Node *c = newNode(NODE_ELEMENT);
    c->c.element = ch;
    if (bit == 0) n->c.p.left  = c;
    else          n->c.p.right = c;
}
static Node* generateTreeFromTable() {
    Node* master = newNode(NODE_PARENT);
    Symbol *s = symbols;
    for (int i = 0; i < 256; ++i) {
        if (s->length) insertSymbolInTree(master, (char)i, s);
        ++s;
    }
    return master;
}

// The final step...
static void writeDecompContent(FILE* inf, FILE* outf,
                               Node* master, uint32_t size) {
    byte bitbuf = fgetc(inf), bitpos = 0;
    uint32_t i = 0; Node *n = master;
    while (i < size) {
        if (bitpos == 8) {
            bitpos = 0; bitbuf = fgetc(inf);
        }
        if ((bitbuf & (1 << bitpos)) == 0) n = n->c.p.left;
        else                               n = n->c.p.right;

        if (n->type == NODE_ELEMENT) {
            fputc(n->c.element, outf); n = master; ++i;
        }
        ++bitpos;
    }
}

/////////////////////////////////

static void countCharacters(FILE *f) {
    for(int c=fgetc(f);c!=EOF;c=fgetc(f)){charcount[c]++;}rewind(f);}
static FILE* xfopen(const char* fn, const char* t) {
    FILE* f = fopen(fn, t); if (!f) pdie("fopen"); return f; }
int main(int argc, const char **argv) {
    char* tmp; int verbose = 0, outfnMalloc = 0;
    // Checks for arguments
    if (argc != 3 && argc != 4) {
        puts("USAGE: huffman (c|d) INPUT [OUTPUT]");
        return 1;
    }
    // Compress or decompress mode?
    // input and output files
    const char* infn  = argv[2];
    // const char* outfn = argc == 4 ? argv[3] : "out.bin";
    const char* outfn;
    if (argc == 4) outfn = argv[3];
    else {
        outfnMalloc = 1; // So I can free it at the end
        switch (*argv[1]) {
        case 'c':
            tmp = (char*)malloc(sizeof(char) * (strlen(infn) + 5));
            sprintf(tmp, "%s.hmc", infn);
            break;
        case 'd':
            tmp = (char*)malloc(sizeof(char) * (strlen(infn)));
            strcpy(tmp, infn);
            tmp[strlen(tmp) - 4] = 0; // roflmao
            break;
        }
        outfn = tmp;
    }

    // Checks the VERBOSE env var
    tmp = getenv("VERBOSE");
    if (tmp && tmp[0] == '1') verbose = 1;

    FILE* inf  = xfopen(infn, "r");
    FILE* outf = xfopen(outfn, "w");

    uint32_t size;
    Node *master;
    switch (*argv[1]) {
    case 'c':
        // Compression code...
        countCharacters(inf);
        Node *n;
        master = newNode(NODE_LIST);
        for (int i = 0; i < 256; ++i) {
            if (charcount[i]) {
                n = newNode(NODE_ELEMENT);
                n->weight = charcount[i];
                n->c.element = i;
                insertNodeByWeight(master, n);
            }
        }
        generateTree(master);
        if (verbose) printTree(master->next, 0);
        generateTable(master);
        if (verbose) printTable();

        generateCompressedFile(inf, outf);

        freeTable();
        break;
    case 'd':
        if (!isValidFile(inf)) die("This is not a valid file.");

        readFileInfo(inf, &size);
        master = generateTreeFromTable();

        if (verbose) {
            printTree(master, 0);
            printTable();
        }

        writeDecompContent(inf, outf, master, size);
        break;
    default: die("Invalid argument"); break;
    }

    if (outfnMalloc) free((char *)outfn); // cursed
    fclose(inf); fclose(outf);
    return 0;
}
