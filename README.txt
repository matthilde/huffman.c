huffman.c by matthilde

This is an implementation of the Huffman Coding algorithm, which is a lossless
compression algorithm, it is used in the DEFLATE algorithm and many others.
    This project has no intent to be improved into an actual compression
algorithm as it is just a simple learning project to practice some programming
concepts and learn how the Huffman Coding works.

USAGE
-----

If you wish to actually use that tool, here's how you use it:
    huffman c INPUTFILE [OUTPUTFILE]
        compresses the INPUTFILE and writes it to OUTPUTFILE. if no output
        file has been specified, it will default to INPUTFILE.hmc.
    huffman d INPUTFILE [OUTPUTFILE]
        takes an INPUTFILE that has been compressed with this tool and
        decompresses its content to OUTPUTFILE. If no file has been specified,
        it will also default to INPUTFILE minus 4 characters (yes i assume the
        file has .hmc as file extension).

There is also a VERBOSE environment variable you can use in case you want to
see the generated symbol table and huffman tree for some reason. You just need
to set it to 1.
 $ VERBOSE=1 ./huffman [your arguments]

BUILDING
--------

Building that tool is fairly simple. You just have to type this command:
 $ gcc -Wall -Wextra -pedantic -std=c99 -o huffman huffman.c
you can also type this shorter command if you wanna avoid some warnings popping
up:
 $ gcc -std=c99 -o huffman huffman.c

NOTES ABOUT THE SOURCE CODE
---------------------------

Since this is a learning project, I haven't actually bothered commenting much
the code. I'm sorry if this gives inconvenience. But almost everything has
been factored into a bunch of functions and not one huge block of code so it
should be fine to read i guess.

LICENSE
-------

This project belongs to the public domain. Do whatever you want with it I don't
care.
